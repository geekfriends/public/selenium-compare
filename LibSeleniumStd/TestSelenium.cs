﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;

namespace LibSeleniumStd
{
    public static class Test
    {
        public static void RunTest()
        {
            String searchString = "Hello World";

            //ChromeDriverService service = ChromeDriverService.CreateDefaultService(@Directory.GetCurrentDirectory(), "chromedriver.exe");   
            //EdgeDriverService service = EdgeDriverService.CreateDefaultService(@Directory.GetCurrentDirectory(), "MicrosoftWebDriver.exe");
            //InternetExplorerDriverService service = InternetExplorerDriverService.CreateDefaultService(@Directory.GetCurrentDirectory(), "IEDriverServer.exe");
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@Directory.GetCurrentDirectory(), "geckodriver.exe");

            //using (IWebDriver driver = new ChromeDriver(service))
            //using (IWebDriver driver = new EdgeDriver(service))
            //using (IWebDriver driver = new InternetExplorerDriver(service))
            using (IWebDriver driver = new FirefoxDriver(service))
            {
                //Launch the ToolsQA Website
                driver.Navigate().GoToUrl("http://www.google.fr");

                // Find the text input element by its name
                IWebElement query = driver.FindElement(By.Name("q"));

                // Enter something to search for
                query.SendKeys(searchString);

                // Now submit the form. WebDriver will find the form for us from the element
                query.Submit();

                // Google's search is rendered dynamically with JavaScript.
                // Wait for the page to load, timeout after 10 seconds
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                wait.Until(d => d.Title.StartsWith(searchString, StringComparison.OrdinalIgnoreCase));

                //Closing browser
                driver.Quit();
            }
        }
    }
}
